module gitlab.com/CRThaze/exiter

go 1.21.3

require (
	gitlab.com/CRThaze/cworthy v0.0.4
	gitlab.com/CRThaze/sigar v0.0.0-20231030111337-e540ad5f924d
)
